/**
 * OrderController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
    neworder: async function(req, res) {
        var usercart = await Shoppingcart.findOne({
            id: req.body.cart
        });

        var sendemail = require('./send-mail');
        var sendtext = require('./send-sms');

        var ordersms = 'New Order created at '+ new Date() + '\n';
        var orderemail = `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <html xmlns="http://www.w3.org/1999/xhtml">
        <head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <title>Zigy Demo Store - New Order</title>
            <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        </head>
        <body style="margin: 0; padding: 0; background-color: #eeeeee">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr><td>
                <table align="center" cellpadding="0" cellspacing="0" width="600" bgcolor = 'white' style="border-collapse: collapse;">
                    <tr height = '100px' bgcolor = '#154767'> <td width = 70% style = 'color: white; font-size: 30px;' align = 'center'>
                            Zigy Demo Store
                    </td> </tr>
                    <tr height = '150px'>
                        <td align = 'center' style = "font-size: 24px;">Thank you for Your Order!</td>
                    </tr>
                    <tr><td>
                        <table align = 'center' width = '90%'>
                            <tr><td>
                                <table align = 'center' cellpadding = '5' width = '100%' style = "border-collapse: collapse">
                                    <thead bgcolor = '#eeeeee'>
                                        <th colspan = '2'>Order details</th>
                                    </thead>`;
        var item;
        var printcart = await Shoppingcart.findOne({
            where: {
                id: req.body.cart,
                owner: req.body.user
            }
        }).populate('product');
        var ordermessage = '';

        for (items in printcart.product) {
            item = printcart.product[items];
            ordersms += '\nItem name: ' + item.name + '\nUnit Price: ₦' + item.price + '\nAmount ordered: ' + item.quantity + '\nPrice: ₦' + item.totalprice + '\n';
            
            orderemail += `<tr>
                            <td> ${item.name} (${item.quantity})</td>
                            <td>₦ ${item.totalprice}</td>
                           </tr>`;
        }

        orderemail += `<tfoot border = "1" style = 'border-top: thick solid #eeeeee; border-bottom: thick solid #eeeeee;'>
                        <td>Total</td>
                        <td>₦ ${usercart.total}</td>
                    </tfoot>
                </table>
                </td></tr>
                </td>
                <tr height = '100px'>
                <td>
                    Order created at ${new Date()}
                </td>
                </tr>
                </table>
                </td></tr>
                </table>
                </body>
                </html>`;
        ordersms += '/nTotal: ' + usercart.total; 

        console.log(ordersms);
        //to get email addr and phone number
        phone = req.body.phone; 
        var email = req.body.email;

        var user = await User.findOne({id:printcart.owner});

        await sendemail(email, orderemail, res); 

        ordermessage += await sendtext(phone, ordersms, res);
        console.log("\nAfter text, Printing order message to be returned to browser ", ordermessage);

        await Order.create({
            owner: user.id,
            cart: printcart.id});
            
        await Shoppingcart.destroy({
            id: printcart.id
        });

        if (res.status === 200) {
            console.log("Status check");
            res.writeHead(200, {'Content-Type': 'text/plain'});
        }
        res.end(".");
        //res.end(ordermessage);
    }
};
