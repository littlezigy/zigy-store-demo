/**
 * UserController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {

/*              Basic User login              *
*--------------------------------------------*/
  login: async function(req, res) {
    req.session.user = {};
    console.log("\n******User Login *********\nChecking if user is logged in and session id: " + req.session.user.loggedin + " " + req.session.user.id);
    //look for username in User model entries
    var finduser = await User.findOne({
      where: {
        username: req.body.username 
      }
    });
    if (finduser == false || finduser == undefined) {
      console.log("Username not found.");
      res.status(404);
      res.send("Username not found.");
    } else {
      finduser = await User.findOne({
        where: {
          username: req.body.username, 
          password: req.body.password 
        }
      });
      //console.log(finduser.id);

      if (finduser == false || finduser == undefined) {
        res.status(404);
        res.send("Invalid credentials");
      } else {
        console.log('--running else--');
        req.session.user.id = finduser.id;
        req.session.user.loggedin = true; 
        console.log("User " + req.session.user.id + "logged in: " + req.session.user.loggedin);
        
        console.log("Checking cart...");
        if (req.session.cart != false) {
          console.log("Session cart exists.", req.session.cart);
        }

        res.send({loggedin: true, uname: req.body.username, loginid: finduser.id, email: finduser.email, phonenumber: finduser.phonenumber});
      }
    }
  },

  
/*                  Admin Login                 *
*----------------------------------------------*/
  adminlogin: async function (req, res) {
    req.session.admin = {};
    var logindetails = req.body;
    var adminusers = await Adminusers.findOne(
      {
          username: logindetails.username,
          password: logindetails.password
      }
    )
    console.log(adminusers);
    req.session.admin.username = adminusers.username;
    req.session.admin.uid = adminusers.id;

    if (adminusers == false || adminusers === undefined || adminusers === []) {
        console.log("No admin user or Admin account with those details exists on this server. Create some now");
        res.send(false);
    } else {        
        req.session.admin.loggedin = true; console.log("Session isadmin boolean checker has been set. " + req.session.admin.loggedin, req.session.admin.username);
        res.send(true);
    }
  },


/*            Logout user             *
*-----------------------------------*/
  logout: function(req, res) {
    console.log("\n******Log out*******", req.session.user);
    if(req.session.user == true || req.session.admin == true) {
      console.log("Logging you out, user " + req.session.user.id);
    } else {
      console.log("You aren't logged in.");
    }
    delete req.session.admin;
    delete req.session.user;
    console.log("Checking if user is logged in ", req.session.user, req.session.admin);
    res.send("Logged out");
  }
};
