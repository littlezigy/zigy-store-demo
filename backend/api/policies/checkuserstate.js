module.exports = async function (req, res, proceed) {
    if (req.session.user) {
        return proceed();
    } else {
        res.status(500);
        res.send("Please log in.");
    }
}